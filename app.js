var _                = require('lodash');
var methodOverride   = require('method-override');
var path             = require('path');
var mongoose         = require('mongoose');
/* Koa App stuff */
var app              = require('koa')();
var route            = require('koa-route');
var passport         = require('koa-passport');
var session          = require('koa-session');
var bodyParser       = require('koa-bodyparser');
var secrets          = require('./config/secrets');
var passportSettings = require('./config/passport');
var apiController    = require('./controllers/api');


mongoose.connect(secrets.db, { server : { autoReconnect: true, socketOptions: { connectTimeoutMS: 10000 } } });
mongoose.connection.on('error', function(err) {
  console.error('MONGO ERROR: Something broke!');
  console.log(err);
});

/* TODO move session data out of the node app */
app.keys = ['Super-Secret-Key', 'My-Other-Key']
app.use(session(app));

// authentication
app.use(passport.initialize())
app.use(passport.session())

app.use(bodyParser());

app.use(function *(next){
  //log start and end
  var start = new Date;
  yield next;
  var ms = new Date - start;
  console.log('%s %s - %s', this.method, this.url, ms);
  this.set("Request-Time", ms+"ms");
});

//add the public routes here
app.use(route.get('/auth/soundcloud', passport.authenticate('soundcloud')));

app.use(route.get("/auth/soundcloud/callback", passport.authenticate('soundcloud', {
    successRedirect: "/success",
    failureRedirect: '/error'
  }
)));

app.use(route.get("/error", function *(){
  this.body = {error: "Something went really wrong :("}
  this.status = 500
}));

app.use(route.get("/success", function *(){
  this.body = {
    success: true,
    code: 200,
    results: "Authorized"
  }
}));

//add auth validation here

//TODO add auth verification middleware here
app.use( function *(next){
  if("here", this.req.isAuthenticated() || this.url === "/"){
    yield next;
  }else{
    this.redirect("/error");
  }
})

//add private routes here

app.use( route.get('/following', apiController.following));

app.use( route.get('/favorites', apiController.favorites) );

app.use( route.put('/follow', apiController.follow) );

//Default route triggers if no other route completes the request
app.use(function *(){
  this.body = {Welcome: 'Soundcloud middle-layer API'};
});

app.listen(1337);
