
var _             = require('lodash');
var async         = require('async');
var secrets       = require('../config/secrets');
var SoundCloudAPI = require("soundcloud-node");
var client        = new SoundCloudAPI(secrets.soundcloud.clientID, secrets.soundcloud.clientSecret, secrets.soundcloud.callbackURL);
var Soundcloud    = require('../models/soundcloud');

var Promise = require('bluebird');
var clientp = Promise.promisifyAll(client);

exports.following = function *(next){

  //TODO clean this up.
  var tokens = this.passport.user.tokens
  var last = tokens[tokens.length-1]

  //TODO invesitage if this is multi-user safe
  client.setToken(last.accessToken);


  var that = this;
  yield new Promise(function(resolve, reject){
    client.get('/users/'+that.passport.user.soundcloud+'/followings', function(err, result){
      if(err) {
        console.log(err);
        that.body = {
          success: false,
          code: 500,
          results: err
        };
      } else {
        that.body =
        {
          success: true,
          code: 200,
          results: result
        };
      }
      resolve();
    });

  });
}

exports.follow = function *(next){

  //TODO clean this up.
  console.log(this);
  var tokens = this.passport.user.tokens
  var last = tokens[tokens.length-1]

  //TODO invesitage if this is multi-user safe
  client.setToken(last.accessToken);

  var that = this;
  yield new Promise(function(resolve, reject){
    client.put('/users/'+that.passport.user.soundcloud+'/followings' + that.body.id, function(err, result){
      if(err) {
        console.log(err);
        that.body = {
          success: false,
          code: 500,
          results: err
        };
        that.status = 500;
      } else {
        that.body =
        {
          success: true,
          code: 200,
          results: result
        };
        that.status = 200;
      }
      resolve();
    });
  });

};

exports.like = function(req, res, next){
  var sc = _.findWhere(req.user.tokens, { kind: 'soundcloud' });
  client.setToken(sc.accessToken);

  client.put('/users/'+req.user.soundcloud+'/favorites' + req.body.id, function(err, result){
    if(err) {
      console.log(err);
      res.json({
        success: false,
        code: '500',
        results: err
      });
    } else {
      console.log(result);
      res.json({
        success: true,
        code: '200',
        results: result
      });
    }
  });

};


exports.favorites = function *(next){
  //TODO clean this up.
  console.log(this.passport);
  var tokens = this.passport.user.tokens
  var last = tokens[tokens.length-1]

  //TODO invesitage if this is multi-user safe
  client.setToken(last.accessToken);

  var that = this;
  yield new Promise(function(resolve, reject){

    function error(msg){
      console.err(msg);
      that.body = {
        success: false,
        code: '500',
        results: err
      };
      resolve();
    }
    
    var r = null;

    client.get('/users/'+that.passport.user.soundcloud+'/favorites', getUserCB)

    function getUserCB(err, result){
      if(err) return error(err);
      r = result;
      Soundcloud.findOne({user: that.passport.user}, findOneCB);
    }

    function findOneCB(err, response){
      if(err) return error(err);
      ifResponse(response);
    }

    function ifResponse(response){
      if(response){return findAndUpdate(response);}
      createAndSave();
    }

    function findAndUpdate(response){
      Soundcloud.findByIdAndUpdate(response.id, {following: r}, function(err, response){
        if (err) return error(err);
        final(r);
      });
    }

    function createAndSave(){
      var soundcloud = new Soundcloud();
      soundcloud.user = that.passport.user;
      soundcloud.favorites = result;
      soundcloud.save(casCB);
    }

    function casCB(err){
      if (err) return error(err);
      final(r);
    }

    function final(result){
      that.body = {
        success: true,
        code: '200',
        results: result
      };
      resolve();
    }
  });

};
