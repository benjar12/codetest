var _                  = require('lodash');
var passport           = require('koa-passport');
var soundcloudStrategy = require('passport-soundcloud').Strategy;
var secrets            = require('./secrets');
var User               = require('../models/User');

exports.isAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) return next();
  res.json({
    success: false,
    code: '200',
    results: 'Not authorized'
  });
};

exports.isAuthorized = function(req, res, next) {
  var provider = req.path.split('/').slice(-1)[0];
  if (_.findWhere(req.user.tokens, { kind: provider })) {
    next();
  } else {
    res.json({code: 401, results: 'You are not authorized'});
  }
};


passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});


passport.use(new soundcloudStrategy(secrets.soundcloud , function(req, accessToken, refreshToken, profile, done) {

  //define a general error handler
  function errHandler(msg){
    console.error(msg);
    done(msg);
  }

  var t =  {kind: 'soundcloud', accessToken: accessToken };

  var prom = User.findOne({ soundcloud: profile.id }).exec();

  prom.addErrback(function () { errHandler("Mongo Error") });

  prom = prom.then(function(user){

      if(!user){
        //return User.create({soundcloud: profile.id, tokens: [t]}).save();
        var user = new User();
        user.soundcloud = profile.id;
        user.tokens.push({ kind: 'soundcloud', accessToken: accessToken });
        return user.save();
      }else{
        //return
        user.tokens.push(t)
        return user.save();
      }
 });

 //TODO figure out why err handling is not working
 //prom.addErrback(function () { errHandler("Mongo Error 2") });

 prom.then(function(user){
  console.log("made it into the next then", user);
  done(null, user);
 });


}));
